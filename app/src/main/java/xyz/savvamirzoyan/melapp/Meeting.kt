package xyz.savvamirzoyan.melapp

class Meeting(
    val teacherName: String,
    val article: String,
    val date: String,
    val time: String,
    val link: String
) {
    fun createTextRepresentation(): String {
        return """
            Zoom
            
            Тема: $article
            Вчитель: $teacherName
            Коли: $time $date
            Посилання: $link
            
            (MelApp - liceum2.mk.ua/app)
        """.trimIndent()
    }
}