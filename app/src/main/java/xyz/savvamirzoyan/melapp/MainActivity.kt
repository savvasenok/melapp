package xyz.savvamirzoyan.melapp

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    // RecycleView
    private lateinit var viewAdapter: HomeworkRecyclerViewAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    var formChoose: Int = 8
    var homeworkDate1: String = ""
    var homeworkDate2: String = ""

    @SuppressLint("SetTextI18n")
    private fun updateHomeworkInfo() {
        val date =
            if (homeworkDate1 == homeworkDate2) homeworkDate1 else "$homeworkDate1 - $homeworkDate2"
        homeworkInfoTextView.text = "$formChoose клас | $date"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewHiderAndRevealer = ViewHiderAndRevealer(this)
        val datePicker = DatePicker(this)
        val homeworkApi = HomeworkApi(this, viewHiderAndRevealer)

        viewHiderAndRevealer.showSearchUI()

        val homeworkArray: Array<Homework> = arrayOf()
        viewManager = LinearLayoutManager(this)
        viewAdapter = HomeworkRecyclerViewAdapter(homeworkArray)

        homeworkRecycleView.layoutManager = viewManager
        homeworkRecycleView.adapter = viewAdapter

        // Views modification on start
        homeworkInfoTextView.text = datePicker.getCurrentDate()

        // Creating Spinners
        createSpinner(this, userFormSpinner, R.array.forms)
        createSpinner(this, homeworkDateSpinner, R.array.homeworkDateArray)


        // Setting up listeners
        //// searchButton listener. Makes progressBar visible while request does its job
        buttonSearch.setOnClickListener {
            homeworkApi.updateHomework(
                formChoose,
                homeworkDate1,
                homeworkDate2
            ) { HWArray ->
                runOnUiThread {
                    viewAdapter.homeworks = HWArray
                    viewAdapter.notifyDataSetChanged()
                }
            }
        }

        buttonReturn.setOnClickListener {
            viewHiderAndRevealer.hideOutputUI()
            viewHiderAndRevealer.showSearchUI()
        }

        //// userFormSpinner listener. Updates formChoose
        userFormSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            @SuppressLint("SetTextI18n")
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                formChoose = parent?.getItemAtPosition(position).toString().toInt()
                updateHomeworkInfo()
            }
        }

        //// homeworkDateSpinner listener
        homeworkDateSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                when (position) {
                    0 -> {
                        // if choosed option is "today", then dates to be requested are the same
                        homeworkDate1 = datePicker.getCurrentDate()
                        homeworkDate2 = homeworkDate1
                    }
                    1 -> {
                        // if choosed option is "tomorrow", then dates to be requested are the same
                        homeworkDate1 = datePicker.getTomorrowDate()
                        homeworkDate2 = homeworkDate1
                    }
                    2 -> {
                        // if choosed option is "current week", then
                        //    homeworkDate1 is this week monday
                        //    homeworkDate2 is this week friday
                        val result = datePicker.getWeekDatesRange()
                        homeworkDate1 = result.first
                        homeworkDate2 = result.second
                    }
                    3 -> {
                        // if choosed option is "next week", then
                        //    homeworkDate1 is next week monday
                        //    homeworkDate2 is next week friday
                        val result = datePicker.getNextWeekDatesRange()
                        homeworkDate1 = result.first
                        homeworkDate2 = result.second
                    }
                }
                updateHomeworkInfo()
            }
        }

        buttonZoom.setOnClickListener {
            val intent = Intent(this, ZoomMeetingsActivity::class.java)
            startActivity(intent)
        }
    }
}

private fun createSpinner(activity: MainActivity, spinner: Spinner, array: Int) {
    // Create an ArrayAdapter using the string array and a default spinner layout
    ArrayAdapter.createFromResource(activity, array, android.R.layout.simple_spinner_item)
        .also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
}
