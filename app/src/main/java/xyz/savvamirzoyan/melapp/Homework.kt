package xyz.savvamirzoyan.melapp

class Homework(
    val subject: String,
    val teacher: String,
    val datesArray: Array<String>,
    val tasksArray: Array<String>
)