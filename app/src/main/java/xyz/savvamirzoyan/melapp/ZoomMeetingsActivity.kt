package xyz.savvamirzoyan.melapp

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_zoom_meetings.*


class ZoomMeetingsActivity : AppCompatActivity() {

    private lateinit var viewAdapter: ZoomMeetingsRecyclerViewAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val noZoomMeetings: String = "Поки що немає зустіч. Подивись пізніше"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoom_meetings)

        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Zoom зустрічі"
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)

        val zoomMeetingsArray: Array<Meeting> = arrayOf()
        viewManager = LinearLayoutManager(this)
        viewAdapter = ZoomMeetingsRecyclerViewAdapter(zoomMeetingsArray)


        ZoomMeetingsRecyclerView.layoutManager = viewManager
        ZoomMeetingsRecyclerView.adapter = viewAdapter

        val zoomMeetingApi = ZoomMeetingApi(this)
        zoomMeetingApi.updateZoomMeetings(zoomMeetingProgressBar) { ZMArray ->
            runOnUiThread {
                viewAdapter.meetings = ZMArray
                viewAdapter.notifyDataSetChanged()

                if (ZMArray.isEmpty()) {
                    Toast.makeText(this, noZoomMeetings, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
