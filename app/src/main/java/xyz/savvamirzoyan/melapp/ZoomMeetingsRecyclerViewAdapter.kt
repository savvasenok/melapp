package xyz.savvamirzoyan.melapp

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView

class ZoomMeetingsRecyclerViewAdapter(var meetings: Array<Meeting>) :
    RecyclerView.Adapter<ZoomMeetingsRecyclerViewAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val meetingsArticleTextView: TextView = itemView.findViewById(R.id.meetingsArticleTextView)
        val teacherNameTextView: TextView = itemView.findViewById(R.id.teacherNameTextView)
        val timeTextView: TextView = itemView.findViewById(R.id.timeTextView)
        val dateTextView: TextView = itemView.findViewById(R.id.dateTextView)
        val openLinkButton: Button = itemView.findViewById(R.id.buttonOpenLink)
        val shareLinkButton: ImageButton = itemView.findViewById(R.id.buttonShareLink)

        val context: Context = view.context
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.zoom_meeting_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val meeting = meetings[position]
        holder.meetingsArticleTextView.text = meeting.article
        holder.teacherNameTextView.text = meeting.teacherName
        holder.timeTextView.text = meeting.time
        holder.dateTextView.text = meeting.date

        holder.openLinkButton.setOnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(meeting.link)
            startActivity(holder.context, openURL, null)
        }

        holder.shareLinkButton.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, meeting.createTextRepresentation())
            startActivity(
                holder.context,
                Intent.createChooser(shareIntent, "Оберіть додаток:"),
                null
            )
        }
    }

    override fun getItemCount(): Int = meetings.size
}
