package xyz.savvamirzoyan.melapp

import android.app.Activity
import android.widget.Toast
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.json.FuelJson
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result


class HomeworkApi(
    private val activity: Activity,
    private val viewHiderAndRevealer: ViewHiderAndRevealer
) {

    private val homeworkApiLink = "https://melapp.savvamirzoyan.xyz/?"
    private val errorMessage = "Нажаль, сервіс зараз не працює"

    fun updateHomework(
        form: Int,
        date1: String,
        date2: String,
        callback: (Array<Homework>) -> Unit
    ) {
        viewHiderAndRevealer.hideSearchUI()
        viewHiderAndRevealer.showProgressBar()

        homeworkApiLink.httpGet(
            parameters = listOf(
                "start" to date1,
                "end" to date2,
                "form" to form
            )
        ).responseJson { _, _, result ->
            when (result) {
                is Result.Success -> {
                    callback(parseGoogleSpreadsheetJson(result.get()))

                    viewHiderAndRevealer.hideProgressBar()
                    viewHiderAndRevealer.showOutputUI()
                }
                is Result.Failure -> {
                    Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show()

                    viewHiderAndRevealer.showSearchUI()
                    viewHiderAndRevealer.hideProgressBar()
                }
            }
        }
    }

    private fun parseGoogleSpreadsheetJson(json: FuelJson): Array<Homework> {
        var homeworkArray: Array<Homework> = arrayOf()

        // Reading json and filling arrays
        val homeworks = json.obj().getJSONArray("homework")
        for (id in 0 until homeworks.length()) {
            val homeworkItem = homeworks.getJSONObject(id)
            var datesArray: Array<String> = arrayOf() // these two arrays are the same size
            var tasksArray: Array<String> = arrayOf() // these two arrays are the same size

            for (i in 0 until homeworkItem.getJSONArray("tasks").length()) {
                val taskDate = homeworkItem.getJSONArray("tasks").getJSONObject(i).getString("date")
                val taskString =
                    homeworkItem.getJSONArray("tasks").getJSONObject(i).getString("task")

                if (taskString.isNotEmpty()) {
                    datesArray += taskDate
                    tasksArray += taskString
                }
            }

            if (tasksArray.isNotEmpty() and (tasksArray.size == datesArray.size)) {
                homeworkArray += Homework(
                    homeworkItem.getString("subject"),
                    homeworkItem.getString("teacher"),
                    datesArray,
                    tasksArray
                )
            }
        }

        return homeworkArray
    }
}