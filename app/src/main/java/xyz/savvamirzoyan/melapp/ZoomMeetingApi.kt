package xyz.savvamirzoyan.melapp

import android.app.Activity
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.json.FuelJson
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result

class ZoomMeetingApi(private val activity: Activity) {

    private val zoomMeetingsApiLink = "https://savvasenok.pythonanywhere.com/getZoomMeetings"
    private val errorMessage = "Нажаль, сервіс зараз не працює"

    fun updateZoomMeetings(progressBar: ProgressBar, callback: (Array<Meeting>) -> Unit) {
        progressBar.visibility = View.VISIBLE
        zoomMeetingsApiLink.httpGet().responseJson { _, _, result ->
            when (result) {
                is Result.Success -> {
                    callback(parseZoomMeetings(result.get()))
                }
                is Result.Failure -> {
                    Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show()
                }
            }

            progressBar.visibility = View.INVISIBLE
        }
    }

    private fun parseZoomMeetings(json: FuelJson): Array<Meeting> {
        var meetingsArray: Array<Meeting> = arrayOf()
        val meetings = json.obj().getJSONArray("meetings")

        for (id in 0 until meetings.length()) {
            val meeting = meetings.getJSONObject(id)
            meetingsArray += Meeting(
                meeting.getString("name"),
                meeting.getString("article"),
                meeting.getString("date"),
                meeting.getString("time"),
                meeting.getString("link")
            )
        }

        return meetingsArray
    }
}