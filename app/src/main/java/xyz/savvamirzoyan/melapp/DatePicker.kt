package xyz.savvamirzoyan.melapp

import android.widget.Toast
import java.util.*

class DatePicker(private val activity: MainActivity) {

    private val dayMonthDivider = "."
    private val weekendReason = "Обраний день є віхидним. Змінено на наступний понеділок"

    private fun formatDateWithZero(num: Int): String = if (num < 10) "0${num}" else num.toString()
    private fun prettifyDate(day: Int, month: Int): String =
        "${formatDateWithZero(day)}$dayMonthDivider${formatDateWithZero(month)}"

    private fun checkForWeekend(date: Calendar): Calendar {
        // if its weekend. Add 1 day if its Sunday and 2 days if it is Saturday

        val dayOfTheWeek = date.get(Calendar.DAY_OF_WEEK)

        if ((dayOfTheWeek == 1) or (dayOfTheWeek == 7)) {
            when (dayOfTheWeek) {
                1 -> date.add(Calendar.DAY_OF_MONTH, 1)
                7 -> date.add(Calendar.DAY_OF_MONTH, 2)
            }
            Toast.makeText(
                activity,
                weekendReason,
                Toast.LENGTH_SHORT
            ).show()
        }
        return date
    }

    fun getCurrentDate(): String {
        val today = checkForWeekend(Calendar.getInstance())
        val day = today.get(Calendar.DAY_OF_MONTH)
        val month = today.get(Calendar.MONTH) + 1

        return prettifyDate(day, month)
    }

    fun getTomorrowDate(): String {
        val today = Calendar.getInstance()
        var tomorrow = today.clone() as Calendar
        tomorrow.add(Calendar.DAY_OF_MONTH, 1)
        tomorrow = checkForWeekend(tomorrow)

        val day = tomorrow.get(Calendar.DAY_OF_MONTH)
        val month = tomorrow.get(Calendar.MONTH) + 1

        return prettifyDate(day, month)
    }

    fun getWeekDatesRange(): Pair<String, String> {
        val currentDate = Calendar.getInstance()
        val firstDayOfWeek = currentDate.clone() as Calendar

        println("DATE: ${currentDate.get(Calendar.DAY_OF_WEEK)}")


        // Calculating first day
        when (currentDate.get(Calendar.DAY_OF_WEEK)) {
            1 -> firstDayOfWeek.add(Calendar.DAY_OF_WEEK, -6)
            else -> firstDayOfWeek.add(
                Calendar.DAY_OF_WEEK,
                -(currentDate.get(Calendar.DAY_OF_WEEK) - 2)
            )
        }
        val firstDateDay = firstDayOfWeek.get(Calendar.DAY_OF_MONTH)
        val firstDateMonth = firstDayOfWeek.get(Calendar.MONTH) + 1

        // Calculating last day
        val lastDayOfWeek = firstDayOfWeek.clone() as Calendar
        lastDayOfWeek.add(Calendar.DAY_OF_WEEK, 4)
        val lastDateDay = lastDayOfWeek.get(Calendar.DAY_OF_MONTH)
        val lastDateMonth = lastDayOfWeek.get(Calendar.MONTH) + 1

        return Pair(
            prettifyDate(firstDateDay, firstDateMonth),
            prettifyDate(lastDateDay, lastDateMonth)
        )
    }

    fun getNextWeekDatesRange(): Pair<String, String> {
        val currentDate = Calendar.getInstance()
        val firstDayOfNextWeek = currentDate.clone() as Calendar

        // Calculating first day of the next week
        when (currentDate.get(Calendar.DAY_OF_WEEK)) {
            1 -> firstDayOfNextWeek.add(Calendar.DAY_OF_WEEK, 1)
            else -> firstDayOfNextWeek.add(
                Calendar.DAY_OF_WEEK,
                9 - currentDate.get(Calendar.DAY_OF_WEEK)
            )
        }
        val firstDateDay = firstDayOfNextWeek.get(Calendar.DAY_OF_MONTH)
        val firstDateMonth = firstDayOfNextWeek.get(Calendar.MONTH) + 1

        // Calculating last day of the next week
        val lastDayOfNextWeek = firstDayOfNextWeek.clone() as Calendar
        lastDayOfNextWeek.add(Calendar.DAY_OF_WEEK, 4)
        val lastDateDay = lastDayOfNextWeek.get(Calendar.DAY_OF_MONTH)
        val lastDateMonth = lastDayOfNextWeek.get(Calendar.MONTH) + 1

        return Pair(
            prettifyDate(firstDateDay, firstDateMonth),
            prettifyDate(lastDateDay, lastDateMonth)
        )
    }
}