package xyz.savvamirzoyan.melapp

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView


@SuppressLint("Registered")
class ViewHiderAndRevealer(activity: Activity) {

    private val buttonSettings: ImageButton = activity.findViewById(R.id.buttonReturn)
    private val buttonSearch: ImageButton = activity.findViewById(R.id.buttonSearch)
    private val buttonZoom: Button = activity.findViewById(R.id.buttonZoom)
    private val homeworkDateSpinner: Spinner = activity.findViewById(R.id.homeworkDateSpinner)
    private val userFormSpinner: Spinner = activity.findViewById(R.id.userFormSpinner)
    private val homeworkDateTextView: TextView = activity.findViewById(R.id.homeworkDateTextView)
    private val chooseUserFormTextView: TextView =
        activity.findViewById(R.id.chooseUserFormTextView)
    private val homeworkInfoTextView: TextView = activity.findViewById(R.id.homeworkInfoTextView)
    private val homeworkRecycleView: RecyclerView = activity.findViewById(R.id.homeworkRecycleView)
    private val progressBar: ProgressBar = activity.findViewById(R.id.progressBar)

    private val searchUIElements: Array<View> = arrayOf(
        buttonSearch, userFormSpinner,
        homeworkDateSpinner, chooseUserFormTextView, homeworkDateTextView, buttonZoom
    )
    private val outputUIElements: Array<View> = arrayOf(buttonSettings, homeworkInfoTextView)

    fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    fun hideSearchUI() {
        for (element in searchUIElements) {
            element.visibility = View.INVISIBLE
        }
    }

    fun hideOutputUI() {
        for (element in outputUIElements) {
            element.visibility = View.INVISIBLE
        }
        homeworkRecycleView.visibility = View.GONE
    }

    fun showSearchUI() {
        for (element in searchUIElements) {
            element.visibility = View.VISIBLE
        }
    }

    fun showOutputUI() {
        for (element in outputUIElements) {
            element.visibility = View.VISIBLE
        }
        homeworkRecycleView.visibility = View.VISIBLE
    }
}