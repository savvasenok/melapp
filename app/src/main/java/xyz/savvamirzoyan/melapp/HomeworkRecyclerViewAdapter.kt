package xyz.savvamirzoyan.melapp

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class HomeworkRecyclerViewAdapter(var homeworks: Array<Homework>) :
    RecyclerView.Adapter<HomeworkRecyclerViewAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewSubject: TextView = itemView.findViewById(R.id.textViewSubject)
        val textViewTeacher: TextView = itemView.findViewById(R.id.textViewTeacher)
        val taskHolder: LinearLayout = itemView.findViewById(R.id.tasksHolder)

        val context: Context = view.context
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.homework_item, parent, false)

        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n", "InflateParams")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val homework = homeworks[position]
        holder.textViewSubject.text = "• ${homework.subject}"
        holder.textViewTeacher.text = "(${homework.teacher})"

        holder.taskHolder.removeAllViews()
        for (i in homework.tasksArray.indices) {
            val taskItemView =
                LayoutInflater.from(holder.context).inflate(R.layout.task_item, null, false)
            val dateTextView: TextView = taskItemView.findViewById(R.id.dateTextView)
            val taskTextView: TextView = taskItemView.findViewById(R.id.taskTextView)
            dateTextView.text = homework.datesArray[i]
            taskTextView.text = homework.tasksArray[i]

            holder.taskHolder.addView(taskItemView)
        }
    }

    override fun getItemCount(): Int = homeworks.size
}
